import React, { FC, useState } from "react";
import { useAuthentication } from "./Authentication";
import { Button, Form, FormControl } from "react-bootstrap";
import config from '../config';
import insuranceLogo from '../assets/images/icons/insurance_2_50.png';

const Login: FC = () => {
  const { loading, login } = useAuthentication();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [shareCredRequestToken] = useState('eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NksifQ.eyJpbnRlcmFjdGlvblRva2VuIjp7ImNyZWRlbnRpYWxSZXF1aXJlbWVudHMiOlt7InR5cGUiOlsiVmVyaWZpYWJsZUNyZWRlbnRpYWwiLCJJRERvY3VtZW50Q3JlZGVudGlhbFBlcnNvblYxIl0sImNvbnN0cmFpbnRzIjpbXX1dLCJjYWxsYmFja1VSTCI6IiJ9LCJleHAiOjE2MjA1NDQxNDE4MTYsInR5cCI6ImNyZWRlbnRpYWxSZXF1ZXN0IiwianRpIjoiZDlhODVkNjcxNTdkYmQ1ZSIsImlzcyI6ImRpZDplbGVtOkVpQmlWdGZqV1RTb0dwNWZfWGNYMms2QTI5Tl9tSlhMcDFzNGpQRlA2NjNTaFE7ZWxlbTppbml0aWFsLXN0YXRlPWV5SndjbTkwWldOMFpXUWlPaUpsZVVwMlkwZFdlVmxZVW5CaU1qUnBUMmxLYW1OdFZtaGtSMVZwVEVOS2NtRlhVV2xQYVVscVkwaEtjR0pYUm5sbFUwbHpTVzFHYzFwNVNUWkphMVpVVFdwVk1sTjVTamtpTENKd1lYbHNiMkZrSWpvaVpYbEtRVmt5T1hWa1IxWTBaRU5KTmtsdGFEQmtTRUo2VDJrNGRtUjZUbkJhUXpWMlkyMWpkbU15Vm1wa1dFcHdaRWhyZG1ScVNXbE1RMHAzWkZkS2MyRlhUa3hhV0d0cFQyeDBOMGx0Ykd0SmFtOXBTVE5DZVdGWE1XaGpibXRwVEVOS01XTXlSbTVhVTBrMlNXNU9jRm95TlhCaWJXTnBURU5LTUdWWVFteEphbTlwVlRKV2FtTkVTVEZPYlhONFZtMVdlV0ZYV25CWk1rWXdZVmM1ZFZNeVZqVk5ha0Y0VDBOSmMwbHVRakZaYlhod1dUQjBiR1ZWYUd4bFEwazJTV3BCZWxwcVZtMVBWR3N3V1RKRmVrOVhWWGxOZW1jMVQxUnJkMDFxUW14UFIxRTFUbTFSZUUweVVteFBSRTE1V1dwRmVVNUhWbWxPUkZKcFRVZE5NMWxYVm10T1YxRjVXbXBXYkUxRVFUTlpla1UwV1ZSRk1FMTVTamxNU0hOcFlWZFJhVTlwU1dwamJWWnFZak5hYkdOdWEybE1RMG94WXpKR2JscFRTVFpKYmtwc1dUSTVNbHBZU2pWSmFYZHBaRWhzZDFwVFNUWkpiRTVzV1ROQmVVNVVXbkpOVmxwc1kyMXNiV0ZYVG1oa1IyeDJZbXQwYkdWVVNYZE5WR2RwVEVOS2QyUlhTbk5oVjA1TVdsaHNTVnBZWjJsUGFVbDNUWHBCZDA5RVpHdE9hbXQzVFVkR2JVMUVUVEZOYWswd1dXMUplVmxYV1RWTlZHUnRXVmROTVUxcVZtMU5WRkYzVFdwb2FWbFVhM2xOYlVWNVdrUlZNbHBIUm1oT1JHZDNXbGRaTUZwdFZtcE9ha3B0VFRKUmFXWldNSE5KYlVZeFpFZG9iR0p1VW5CWk1rWXdZVmM1ZFVscWNHSkphVTUzWTIxc2RGbFlTalZKYkRCelNXMUdlbU15Vm5sa1IyeDJZbXN4YkdSSGFIWmFRMGsyVjNsSmFtTklTbkJpVjBaNVpWTktaR1pSSWl3aWMybG5ibUYwZFhKbElqb2lPVWRNYjJkVVRHeDRVRUp1VG1FNGNVdElWbFZZVFVoYVEybEZkSEZ4VkZSb1dFRmlkRFo0WWt4aldVaFVaM2d0VEROUGNtMVBMUzAxTmxwcFIycHNUWEpzVGxOemNWaDFWMnh5TjJwNVp6bGthVGN0Y1hjaWZRI3ByaW1hcnkifQ.de394ee04bb67bd61e81385ac5c45911f25a745ecfa5cd00b94d1d9c40da37e721d18c286402bafd146cbe0170074ee00836ca8f1bf6715f53377d5acf7e9813');

  async function onLogin() {
    try {
      await login.fromLoginAndPassword(username, password);
    } catch (err) {
      alert(err.message);
    }
  }

  return (
    <div className='Login'>
      <div className='Form'>
      <img src={insuranceLogo} className='logo-insurance' alt='logo-insur'/>
        <h1 className='Title'>Insurer Login</h1>
        <p className='Info'>
          Login in order to continue
        </p>

        <Form style={{ width: 280 }}>
          <Form.Group controlId="username">
            <Form.Label>Username</Form.Label>
            <FormControl
              autoFocus
              type="text"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
          </Form.Group>

          <Form.Group controlId="password">
            <Form.Label>Password</Form.Label>
            <FormControl
              type="password"
              value={password}
              onChange={(event) => setPassword(event.target.value)}
            />
          </Form.Group>

          <Button block disabled={loading} onClick={onLogin}>
            Login
          </Button>
        <br/>
        <br/>
         <p> Patients wishing to share Verifiable Credentials with Insurer <a href={config.wallet_url + '/share-credentials?token=' + shareCredRequestToken} target='_blank' rel="noopener noreferrer">click here!</a></p>
        </Form>
      </div>
    </div>
    
  );
};

export default Login;
