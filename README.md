# Affinidi POCathon Health Insurance Use Case Implementation - Insurance Verifier

## Introduction

Welcome to the Insurance Portal for the Health Insurance Use Case Implementation designed by Dr Kent Lau [https://linkedin.com/in/kentglau].

<br/>
<br/>
<img src="./src/assets/images/icons/insurance.png">